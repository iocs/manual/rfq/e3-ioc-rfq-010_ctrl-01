# Startup for RFQ-010:Ctrl-IOC-01

# Load standard IOC startup scripts
require essioc
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("iocsh/rfq_010_ctrl_plc_01.iocsh")
